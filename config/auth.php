<?php

return [

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'users',
        ],
    ],


    'providers' => [
        'users' => [
            'driver' => 'inmov_auth',
            'model' => Inmovsoftware\LoginApi\Models\V1\Userlogin::class,
        ],

    ],


    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
