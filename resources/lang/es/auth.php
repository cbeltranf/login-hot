<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'El usuario y contraseña ingresados no coinciden con nuestros registros.',
    'emailfailed'   => 'El correo ingresado no se encuentra asociado a ninguna compañia.',
    'logout'   => 'Se ha cerrado sesión con éxito.',
    'inactive'   => 'El usuario se encuentra inactivo.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'test'   => 'Esta es una prueba.',
    'nomatch'   => 'Los datos que ingresaste no coinciden con ninguno de nuestros registros. Por favor, verificala información e inténtalo de nuevo.',
    'smssend'   => 'Hemos enviado un codigo de recuperación de contraseña a tu telefono celular  :cel .',
    'emailsend'   => 'Hemos enviado un codigo de recuperación de contraseña a tu correo electrónico.',
    'smssent'   => 'SMS enviado con éxito.',
    'smsphoneoff'   => 'El teléfono de destino al que intenta llegar está apagado o no está disponible.',
    'smsphonenoexist'   => 'El número de destino al que intenta llegar es desconocido y puede que no exista.',
    'emailsent'   => 'Email enviado con éxito.',
    'hi'   => 'Hola',
    'mailrecovery'   => 'Se ha registrado una petición para cambiar la contraseña de tu cuenta de My Digital Card.',
    'entercode'   => 'Ingresa el siguiente código en tu aplicación:',
    'onlysocial'   => 'Tu usuario solo permite iniciar sesión a través de tu cuenta corporativa de GMail.',
    'onlyuser'   => 'Tu usuario no permite el inicio de sesión mediante cuenta de GMail.',
    'noplatform'   => 'Información de plataforma de usuario no detectada.',
    'profileblocked'   => 'Tu perfil de usuario no tiene permitido el acceso al panel de administración.',
    'recoverycodeexpired'   => 'Codigo de recuperacion expirado, por favor genera uno nuevamente.',
    "heading1"=> 'Olvidaste tu contraseña?',
    "heading2"=> 'Tranquilo, a veces pasa...',
    "mailrecovery1" => 'Hemos recibido tu solicitud para restablecer tu contraseña. Para acceder debes copiar el siguiente código de verificación:',
    "mailrecovery2" => 'Si tu no has solicitado el cambio de tu contraseña,',
    "mailrecoverycancel" =>'cancela este proceso de inmediato.',
    "thanks" => 'Gracias!',
    "follow" => 'Síguenos',
];
