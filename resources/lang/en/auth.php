<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'emailfailed'   => 'The mail entered is not associated with any company.',
    'inactive'   => 'The user is inactive.',
    'logout'   => 'Successfully logged out',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'test'   => 'This is a test.',
    'nomatch'   => 'The data you entered does not match any of our records. Please, verify the information and try again.',
    'smssend'   => 'We have sent a recovery code to your cell phone :cel .',
    'emailsend'   => 'We have sent a recovery code to your email adress.',
    'smssent'   => 'SMS sent succesfully.',
    'smsphoneoff'   => 'The destination handset you are trying to reach is switched off or otherwise unavailable.',
    'smsphonenoexist'   => 'The destination number you are trying to reach is unknown and may no longer exist.',
    'emailsent'   => 'Email sent succesfully.',
    'hi'   => 'Hi',
    'mailrecovery'   => 'A request to change the password for your My Digital Card account has been registered.',
    'entercode'   => 'Enter the following recovery code in your application:',
    'onlysocial'   => 'Your user only allows you to log in through your GMail corporate account.',
    'onlyuser'   => 'Your user does not allow logging in via GMail account.',
    'noplatform'   => 'User platform information not detected.',
    'profileblocked'   => 'Your user profile is not allowed to access the admin panel.',
    'recoverycodeexpired'   => 'Expired recovery code, please generate one again.',
    "heading1"=> 'Lost Password?',
    "heading2"=> 'Ok, sometimes happens...',
    "mailrecovery1" => 'You have requested us to re-establish your password. To enter you must copy the following verification code:',
    "mailrecovery2" => 'If you have not requested it,',
    "mailrecoverycancel" =>'get in touch with us immediately.',
    "thanks" => 'Thank you!',
    "follow" => 'Follow Us',


];
