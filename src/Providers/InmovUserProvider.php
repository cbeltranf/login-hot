<?php

namespace Inmovsoftware\LoginApi\Providers;

use Illuminate\Auth\EloquentUserProvider as UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Session;



class InmovUserProvider extends UserProvider {

    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];
        if(Session::has('google_token') && !empty(Session::get('google_token')) && !is_null(Session::get('google_token'))){
            Session::forget('google_token');
            return true;
        }else{
            return $this->hasher->check($plain, $user->getAuthPassword());
        }
/*      $hashed = $user->getAuthPassword();
        if (strlen($hashed) === 0) {
            return false;
        }

        if($hashed === md5($plain)){
            return true;
        }else{
            return false;
        }*/
    }

}
