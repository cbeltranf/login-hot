<?php

namespace Inmovsoftware\LoginApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\LoginApi\Exceptions\ErrorHandler;
use Inmovsoftware\LoginApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Log;

class LoginApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */

    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];


    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'inmov');

        $this->publishes([
            __DIR__.'/../../Config/administrators.json' => config_path('/administrators.json'),
                ], 'twilioadmins');

        Artisan::call('vendor:publish' , [
            '--tag' => 'inmov',
            '--force' => true,
        ]);

        Artisan::call('vendor:publish' , [
            '--tag' => 'twilioadmins',
            '--force' => true,
        ]);
        Artisan::call('vendor:publish' , [
            '--provider' => '"Tymon\JWTAuth\Providers\LaravelServiceProvider"',
            '--force' => true,
        ]);

       $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'login');

       $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
       $this->mergeConfigFileFrom(__DIR__ . '/../../config/auth.php', 'auth');
       $this->mergeConfigFileFrom(__DIR__ . '/../../config/filesystems.php', 'filesystems');

    }


    public function register()
    {
         $env_update = $this->changeEnv([
            'MAIL_DRIVER'=>'smtp',
            'MAIL_HOST'=>'smtp.sendgrid.net',
            'MAIL_PORT'=>'587',
            'MAIL_USERNAME'=>'devinmov',
            'MAIL_PASSWORD'=>'22222',
            'MAIL_API'=>'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',
            'MAIL_ENCRYPTION'=>'tls',
            'MAIL_FROM_NAME'=>'"My_Digital_Card"',
            'MAIL_FROM_ADDRESS'=>'"noreply@mydigitalcard.us"',
            'TWILIO_AUTH_TOKEN'   => '6929e00896ac5f699a85ac88f2687810',
            'TWILIO_ACCOUNT_SID'   => 'ACdc935164b11de2860ce4f34b0f77b8b3',
            'TWILIO_NUMBER'       => '+14697784007']);
        //if($env_update){
            // Do something
        //} else {
            // Do something else
        //}

        $this->loadMiddleware();
        $this->registerExceptionHandler();
        $this->app->make('Inmovsoftware\LoginApi\Models\V1\Userlogin');
        $this->app->make('Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController');


    }

    protected function mergeConfigFileFrom($path, $key)
    {
        $original = $this->app['config']->get($key, []);

        $this->app['config']->set($key, $this->multi_array_merge(require $path, $original, $key));

    }

    protected function multi_array_merge($toMerge, $original, $a)
    {
        $auth = [];

        foreach ($original as $key => $value) {
            if (isset($toMerge[$key])) {

                if($a == "filesystems" && !array_key_exists($key, $auth)){
                    $auth[$key] = '';
                }
                    $auth[$key] = array_merge($value, $toMerge[$key]);
            } else {
                $auth[$key] = $value;
            }
        }
        return $auth;
    }

    protected function loadMiddleware()
    {
        app()->make('router')->aliasMiddleware('jwt',  \Inmovsoftware\LoginApi\Http\Middleware\jwtMiddleware::class);

        $kernel = $this->app->make('Illuminate\Contracts\Http\Kernel');
        $kernel->pushMiddleware('Inmovsoftware\LoginApi\Http\Middleware\CorsInmov');
        $kernel->pushMiddleware('\Inmovsoftware\LoginApi\Http\Middleware\Setlanguageinmov');

    }

    protected function registerExceptionHandler()
    {
        \App::singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            ErrorHandler::class
        );

        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }



    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);
/*
            Log::error(
                'ERR data ' .print_r($data, true)
            );
            Log::error(
                'ERR env ' .print_r($env, true )
            );
*/
            $temp ='';
            foreach((array)$data as $key => $value){
                foreach($env as $env_key => $env_value){
                    if(!empty(trim($env_value))){
                    $temp = explode("=", $env_value, 2);
                        $to_analyze[$temp[0]] = $temp[1];
                    }

                }
            }

/*            Log::error(
                'To analyze  ' .print_r($to_analyze, true )
            );

*/
            // Loop through given data
            foreach((array)$data as $key => $value){

                if (! array_key_exists($key, $to_analyze)) {
                    $env[$key] = $key . "=" . $value;
                                    }

                // Loop through .env-data
                foreach($env as $env_key => $env_value){


                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            return true;
        } else {
            return false;
        }
    }

}
