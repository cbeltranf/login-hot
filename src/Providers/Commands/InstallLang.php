<?php

namespace Inmovsoftware\LoginApi\Providers\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallLang extends Command
{
    protected $signature = 'inmovsoftware:install-inmov';
    protected $description = 'Instalación/Actualización los ficheros de Idioma';

    public function handle()
    {
        Artisan::call('vendor:publish' , [
            '--tag' => 'inmov'
        ]);

        $this->info('Idiomas actualizados. CarlosBeltran.');
    }
}
