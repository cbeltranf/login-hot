<?php
use Illuminate\Http\Request;

Route::group([
    'middleware' => ['api'],
    'prefix' => 'api/v1'
], function () {
    Route::get('locale/{locale}', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@locale');
    Route::post('send/sms', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@sms');
    Route::post('send/email', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@email');
    Route::post('app/version', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@app_version');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'api/v1/auth'
], function () {
    Route::post('login', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@login');
    Route::post('login_social', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@login_social');
    Route::get('logout', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@logout');
});

Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'api/v1/auth'
    ], function () {
        Route::get('me', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@me');
        Route::post('uuid', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@update_uuid');
        Route::get('refresh', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@refresh');
        Route::get('invalidate', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@invalidate');
    });
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'api/v1/app'
], function () {
    Route::post('login', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@login_app');
    Route::post('recovery/pass', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@recovery_password');
    Route::get('recovery/pass/cancel/{code}', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@recovery_password_cancel');
    Route::post('validate/token', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@validate_token');
    Route::get('logout', 'Inmovsoftware\LoginApi\Http\Controllers\V1\AuthController@logout_app');
});
