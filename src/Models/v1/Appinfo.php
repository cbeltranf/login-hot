<?php

namespace Inmovsoftware\LoginApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appinfo extends Model
{
    protected $table = "cyc_etapas";
    protected $primaryKey = 'type';
    protected $guarded = ['type'];
    protected $fillable = ['extra','link_update'];


}
