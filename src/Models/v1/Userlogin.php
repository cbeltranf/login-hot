<?php

namespace Inmovsoftware\LoginApi\Models\V1;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Userlogin extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;
    protected $table = "vs_login";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'last_name',  'phone','email', 'it_business_id', 'avatar_path', 'it_groups_users_id', 'it_profile_id', 'languaje', 'first_login', 'last_login'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
}
