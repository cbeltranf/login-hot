<?php

namespace Inmovsoftware\LoginApi\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;


class Setlanguageinmov
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local =  ($request->hasHeader('X-Language')) ? $request->header('X-Language') : 'en';
        $local = strtolower(trim($local));

       /* Log::error(
            " NEW IM ---------------------------------------------- ". $local
        );*/

        app()->setLocale($local);
        Carbon::setLocale($local);

        return $next($request);
    }
}
