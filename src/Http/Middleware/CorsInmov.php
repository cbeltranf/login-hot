<?php

namespace Inmovsoftware\LoginApi\Http\Middleware;

use Closure;

class CorsInmov
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    return $next($request)
    ->header('Access-Control-Allow-Origin', '*')
    ->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
    ->header('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Cache-Control, Origin, Content-Type, X-Auth-Token, authorization, Authorization, X-Requested-With, X-language, x-language, X-Language, X-Platform, x-platform, X-platform')
    ->header('Access-Control-Expose-Headers', 'Access-Control-Allow-Origin');

    }
}
