<?php
namespace Inmovsoftware\LoginApi\Http\Controllers\V1;

use Inmovsoftware\LoginApi\Models\V1\Userlogin as IT_User;
use Inmovsoftware\UserApi\Models\V1\User as IT_User_Model;
use Inmovsoftware\ProfileApi\Models\V1\Profile;
use Inmovsoftware\LoginApi\Models\V1\Appinfo;
use Illuminate\Support\Facades\Storage;
use Twilio\Exceptions\TwilioException;
use App\Http\Controllers\Controller;
use SendGrid\Mail\Mail as Mail;
use Illuminate\Http\Request;
use SendGrid as SendGrid;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Session;
use JWTAuth;
use Redirect;
use Log;
use DB;


class AuthController extends Controller
{
    private $phone_number  = '';


    public function update_uuid(Request $request)
    {

        $data = $request->validate([
            "id" => "required",
            "uuid" => "required",
            "platform" => "required",
            "model" => "required",
            "version" => "required"
        ]);

        $user =  IT_User_Model::find($data["id"]);
        $user->uuid = $data["uuid"];
        $user->platform = $data["platform"];
        $user->model = $data["model"];
        $user->version = $data["version"];
        $user->save();

        return response()->json($user);
    }

    public function email(Request $request)
    {


        $data = $request->validate([
            "name" => "required",
            "email" => "required|email",
            "message" => "required"
        ]);

        $data_replace = [
            "message" => $data["message"],
        ];

        $email = new Mail();
        $email->setFrom(env("MAIL_FROM_ADDRESS"), "My Digital Card");
        $email->setSubject("My Digital Card - My Bussiness Card");
        $email->addTo($data["email"], $data["name"]);
        $email->setTemplateId("d-22817492af714c2eb487b9eb3c91c01d");
        $email->addDynamicTemplateDatas($data_replace);

        $sendgrid = new SendGrid(env("MAIL_API"));
        $response = $sendgrid->send($email);


        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => [trans('auth.emailsent')]
                ]
            ],
            200
        );
    }


    public function sms(Request $request)
    {

        $data = $request->validate([
            "phone" => "required",
            "message" => "required"
        ]);


        $item[] =   array("name" => "Guest", "phone_number" => $data["phone"]);


        $this->pushSms($data["message"], $item);

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'messages' => [trans('auth.smssent')]
                ]
            ],
            200
        );
    }

    public function recovery_password_cancel(Request $request, $code)
    {

        $cnt = IT_User_Model::where("recovery_code", "=", $code)->count();
        if ($cnt > 0) {
            $us = IT_User_Model::where("recovery_code", "=", $code)->first();

            $user = IT_User_Model::find($us->id);
            $user->date_password_change = NULL;
            $user->recovery_code = NULL;
            $user->save();
            $value = 1;
        } else {
            $value = 0;
        }

        $url = 'https://dashboard.mydigitalcard.us/login?recovery_cancel=' . $value;
        return Redirect::to($url);
    }

    public function recovery_password(Request $request)
    { //Cel y Email
        $data = $request->validate([
            "item" => "required|email",
            "method" => "required|in:email,sms"
        ]);
        $item = request(['item']);
        //dd($item['item']);
        $phone = IT_User_Model::where("phone", "=", $item['item'])->count();
        $email = IT_User_Model::where("email", "=", $item['item'])->count();

        $recovery_token = random_int(111111, 999999);
        if ($phone > 0) {

            $user = IT_User_Model::where("phone", "=", $item['item'])->first();

            if ($user->session_type == "S") {

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.onlysocial')]
                        ]
                    ],
                    401
                );
            }




            $user->recovery_code = $recovery_token;
            $user->date_password_change = Carbon::now();
            $user->save();



            $prefix = request(['prefix']);



            $to_send[] =   array("name" => $user->name . ' ' . $user->last_name, "phone_number" =>  $prefix['prefix'] . $item['item']);

            $this->phone_number = $prefix['prefix'] . str_repeat('*', strlen($item['item']) - 4) . substr($item['item'], -4);

            return $this->pushSms(trans('messages.smsrecovery') . $recovery_token, $to_send);

        } else if ($email > 0) {



            $user = IT_User_Model::where("email", "=", $item['item'])->first();

            if ($user->session_type == "S") {

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.onlysocial')]
                        ]
                    ],
                    401
                );
            }


            if ($data["method"] == "sms") {

                if (empty($user->phone)) {

                    return response()->json(
                        [
                            'errors' => [
                                'status' => 401,
                                'messages' => [trans('exceptions.nocellphoneregistered')]
                            ]
                        ],
                        401
                    );
                }


                $user->recovery_code = $recovery_token;
                $user->date_password_change = Carbon::now();
                $user->save();

                $prefix = $user->country_phone;

                $to_send[] =   array("name" => $user->name . ' ' . $user->last_name, "phone_number" =>  $prefix . $user->phone);
                if (strlen($user->phone) <= 5) {
                    $this->phone_number = $prefix . $user->phone;
                } else {
                    $this->phone_number = $prefix . str_repeat('*', strlen($user->phone) - 4) . substr($user->phone, -4);
                }
                return $this->pushSms(trans('messages.smsrecovery') . $recovery_token, $to_send);
            } else {

                if ($user->session_type == "S") {

                    return response()->json(
                        [
                            'errors' => [
                                'status' => 401,
                                'messages' => [trans('auth.onlysocial')]
                            ]
                        ],
                        401
                    );
                }

                $user->recovery_code = $recovery_token;
                $user->date_password_change = Carbon::now();
                $user->save();

                $data_replace = [
                    "heading_1" => trans('auth.heading1'),
                    "heading_2" => trans('auth.heading2'),
                    "greeting" => trans('auth.hi') . ', ' . $user->name . ' ' . $user->last_name,
                    "message_1" => trans('auth.mailrecovery1'),
                    "message_2" => trans('auth.mailrecovery2'),
                    "code" => $recovery_token,
                    "cancel" => trans('auth.mailrecoverycancel'),
                    "farewell" => trans('auth.thanks'),
                    "follow" => trans('auth.follow')
                ];



                $email = new Mail();
                $email->setFrom(env("MAIL_FROM_ADDRESS"), "My Digital Card");
                $email->setSubject("My Digital Card - Recovery Password");
                $email->addTo($item['item'], $user->name . ' ' . $user->last_name);
                $email->setTemplateId("d-bdb55c44021146a287b72a5b026c0f17");
                $email->addDynamicTemplateDatas($data_replace);
                // $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
                //$email->addContent(
                //    "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
                //);

                //try {
                $sendgrid = new SendGrid(env("MAIL_API"));
                $response = $sendgrid->send($email);
                //print $response->statusCode() . "\n";
                //print_r($response->headers());
                //print $response->body() . "\n";
                //} catch (Exception $e) {
                //    return  'Caught exception: '. $e->getMessage() ."\n";
                //}

                return response()->json(
                    [
                        'errors' => [
                            'status' => 200,
                            'messages' => [trans('auth.emailsend')]
                        ]
                    ],
                    200
                );
            }

        } else {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.nomatch')]
                    ]
                ],
                401
            );
        }
    }

    public function validate_token(Request $request)
    { //Cel y Email
        $data = $request->validate([
            "item" => "required",
            "token" => "required"
        ]);

        $item = request(['item', 'token']);
        $phone = IT_User_Model::where("phone", "=", $item['item'])->where("recovery_code", "=", $item['token'])->count();
        $email = IT_User_Model::where("email", "=", $item['item'])->where("recovery_code", "=", $item['token'])->count();

        $temp_pass = random_int(111111, 999999);
        if ($phone > 0 || $email > 0) {

            $user = IT_User_Model::where("phone", "=", $item['item'])->orWhere("email", "=", $item['item'])->first();

            $carbon1 = new Carbon($user->date_password_change);
            $carbon2 = Carbon::now();

            if ($carbon1->diffInHours($carbon2) > 24) {

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.recoverycodeexpired')]
                        ]
                    ],
                    401
                );
            }

            $user->recovery_code = null;
            $user->first_login = null;
            $user->password = bcrypt($temp_pass);
            $user->save();

            return response()->json(
                [
                    'response' => [
                        'status' => 200,
                        'messages' => [$temp_pass],
                        'email' => $user->email
                    ]
                ],
                200
            );
        } else {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.nomatch')]
                    ]
                ],
                401
            );
        }
    }


    public function locale($locale, Request $request)
    {

        $locale = strtolower(trim($locale));

        if (!empty($locale)) {
            $request->session()->put('locale', $locale);
        }
    }

    public function app_version()
    {
        $app = Appinfo::get();
        return $app;
    }

    public function login(Request $request)
    {

        $data = $request->validate([
            "login" => "required|max:800",
            "password" => "required|max:200",
            'language' => "nullable|max:200",
        ]);

        $plat = $request->header('X-Platform');

        if (!isset($plat) || empty($plat)) {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.noplatform')]
                    ]
                ],
                401
            );
        }


        if (!$this->check_active($data['login'])) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.inactive')]
                        ]
                    ],
                    401
                );
            }

        $soc = $this->is_social($data['login'], $request);

        if ($soc == "true") {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.onlysocial')]
                    ]
                ],
                401
            );
        } else if ($soc == "false") { } else {
            return $soc;
        }


        $credentials = request(['login', 'password']);
        $credentials['status'] = 'A';


        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.failed')]
                    ]
                ],
                401
            );
        }

        $Auth_user = auth('api')->user();
        $MyUser = IT_User::find($Auth_user->id)->toArray();
        $response = array();

        $user = IT_User_Model::find($MyUser["id"]);
        if (is_null($MyUser["first_login"])) {
            $user->first_login = Carbon::now();
        }
        $user->last_login = Carbon::now();
        $user->save();

        $response = $this->clearResponse($MyUser);
        $response["company"] = DB::table('it_business as c')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->select('c.id as id', 'c.name as name', 'c.alias as alias', 'c.logo as logo',  'c.backgroundcolor as backgroundcolor', 'c.textcolor as textcolor')
            ->get();
        $response["position"] = DB::table('it_positions')
            ->where('id', '=', $MyUser["it_positions_id"])
            ->select('id', 'name')
            ->get();
        $response["profile"] = DB::table('it_profiles')
            ->where('id', '=', $MyUser["it_profile_id"])
            ->select('id', 'level', 'name', 'it_business_id', 'status')
            ->get();

        $response["can_change_position"] = DB::table('it_parameters as p')
            ->join('it_business as c', 'c.id', '=', 'p.it_business_id')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->where('p.name', '=', 'change_positions')
            ->select('p.value as value')
            ->get();
        $response["can_change_position"] = $response["can_change_position"][0]->value;


        $response["expires_in"] = auth('api')->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;


        $plat = $request->header('X-Platform');

        if (!isset($plat) || empty($plat) || $plat != 'panel') {
            // $response["card_url"] = $this->make_bitly_url("http://card.getdc.us/?user=".$MyUser["id"]);
            $response["card_url"] = "http://card.getdc.us/?user=" . $MyUser["id"];
        }


        return response()->json($response);
    }


    public function login_social(Request $request)
    {

        $lang = request(['language']);

        $data = $request->validate([
            "email" => "required|email",
            "image" => "nullable|max:900",
            "google_token" => "required",
            'platform' => "nullable|max:40",
            'version' => "nullable|max:30",
            'model' => "nullable|max:30",
            'uuid' => "nullable|max:200",
            'language' => "nullable|max:200",
        ]);



        $plat = $request->header('X-Platform');

        if (!isset($plat) || empty($plat)) {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.noplatform')]
                    ]
                ],
                401
            );
        }

        $email_up = IT_User::where('email', '=', $data['email'])->count();

        if ($email_up < 1) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 422,
                            'messages' => [trans('auth.emailfailed')]
                        ]
                    ],
                    401
                );
            }



        if (!$this->check_active($data['email'])) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.inactive')]
                        ]
                    ],
                    401
                );
            }

        $soc = $this->is_social($data['email'], $request);

        if ($soc == "false") {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.onlyuser')]
                    ]
                ],
                401
            );
        } else if ($soc == "true") { } else {
            return $soc;
        }

        $us = IT_User::where('email', '=', $data['email'])->firstOrFail();
        $credentials = request(['email']);
        $credentials['password'] = $us->password;
        Session::put('google_token', request(['google_token']));
        $credentials['status'] = 'A';

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.failed')]
                    ]
                ],
                401
            );
        }
        $path = '';
        if (!empty($data["image"])) {
            $file_content = file_get_contents($data["image"]);
            $extension = pathinfo(parse_url($data["image"], PHP_URL_PATH), PATHINFO_EXTENSION);
            $filename  = pathinfo(parse_url($data["image"], PHP_URL_PATH), PATHINFO_FILENAME);
            $filename  = preg_replace("/[^A-Za-z0-9]+/", "", strtolower($filename));
            $path = 'profile_images/' . $filename . '_' . time() . '.' . $extension;
            Storage::disk('public')->put($path, $file_content);
        }

        $Auth_user = auth('api')->user();
        $MyUser = IT_User::find($Auth_user->id)->toArray();
        $response = array();

        $update_info_needed = 0;
        $user = IT_User_Model::find($MyUser["id"]);
        if (!empty($data["image"])) {
            $user->avatar = $path;
        }
        if (is_null($MyUser["first_login"])) {
            $user->first_login = Carbon::now();
            $update_info_needed = 1;
        }

        if (!empty($data['platform'])) {
            $user->platform = $data['platform'];
        }
        if (!empty($data['model'])) {
            $user->model = $data['model'];
        }
        if (!empty($data['version'])) {
            $user->version = $data['version'];
        }
        //if(!empty($data['uuid'])){ $user->uuid = $data['uuid'];}
        if (!empty($data['language'])) {
            $user->languaje = $data['language'];
        }

        $user->last_login = Carbon::now();
        $user->save();
        if (!empty($MyUser["recovery_code"]) || !is_null($MyUser["recovery_code"])) {
            $update_info_needed = 1;
        }

        $response = $this->clearResponse($MyUser);
        $response["company"] = DB::table('it_business as c')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->select('c.id as id', 'c.name as name', 'c.alias as alias', 'c.logo as logo',  'c.backgroundcolor as backgroundcolor', 'c.textcolor as textcolor')
            ->get();
        $response["position"] = DB::table('it_positions')
            ->where('id', '=', $MyUser["it_positions_id"])
            ->select('id', 'name')
            ->get();
        $response["profile"] = DB::table('it_profiles')
            ->where('id', '=', $MyUser["it_profile_id"])
            ->select('id', 'level', 'name', 'it_business_id', 'status')
            ->get();

        $response["can_change_position"] = DB::table('it_parameters as p')
            ->join('it_business as c', 'c.id', '=', 'p.it_business_id')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->where('p.name', '=', 'change_positions')
            ->select('p.value as value')
            ->get();
        $response["can_change_position"] = $response["can_change_position"][0]->value;

        $response["expires_in"] = auth('api')->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;
        $response["update_info"] = $update_info_needed;


        $plat = $request->header('X-Platform');

        if (!isset($plat) || empty($plat) || $plat != 'panel') {
            // $response["card_url"] = $this->make_bitly_url("http://card.getdc.us/?user=".$MyUser["id"]);
            $response["card_url"] = "http://card.getdc.us/?user=" . $MyUser["id"];
        }


        return response()->json($response);
    }

    public function login_app(Request $request)
    {
        $lang = request(['language']);
        /*if(isset($lang['language']) && !empty($lang['language']) && $lang['language'] == 'ES'){
            $this->locale($lang['language'], $request);
        }else{
            $this->locale('en', $request);
        }*/

        $data = $request->validate([
            "login" => "required|max:800",
            "password" => "required|max:200",
            'platform' => "required|max:40",
            'version' => "required|max:30",
            'model' => "required|max:30",
            'uuid' => "required|max:200",
            'language' => "nullable|max:200",
        ]);



        if (!$this->check_active($data['login'])) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [trans('auth.inactive')]
                        ]
                    ],
                    401
                );
            }

        $soc = $this->is_social($data['login'], $request);

        if ($soc == "true") {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.onlysocial')]
                    ]
                ],
                401
            );
        } else if ($soc == "false") { } else {
            return $soc;
        }



        $credentials = request(['login', 'password']);
        $credentials['status'] = 'A';
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.failed')]
                    ]
                ],
                401
            );
        }

        $Auth_user = auth('api')->user();
        $MyUser = IT_User::find($Auth_user->id)->toArray();

        $user = IT_User_Model::find($MyUser["id"]);
        if (is_null($MyUser["first_login"])) {
            $user->first_login = Carbon::now();
        }

        $user->platform = $data['platform'];
        $user->model = $data['model'];
        $user->version = $data['version'];
        //$user->uuid = $data['uuid'];
        $user->languaje = $data['language'];
        $user->save();

        $response = array();
        $response = $this->clearResponse($MyUser);
        $response["company"] = DB::table('it_business as c')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->select('c.id as id', 'c.name as name', 'c.alias as alias', 'c.logo as logo',  'c.backgroundcolor as backgroundcolor', 'c.textcolor as textcolor')
            ->get();
        $response["position"] = DB::table('it_positions')
            ->where('id', '=', $MyUser["it_positions_id"])
            ->select('id', 'name')
            ->get();
        $response["profile"] = DB::table('it_profiles')
            ->where('id', '=', $MyUser["it_profile_id"])
            ->select('id', 'level', 'name', 'it_business_id', 'status')
            ->get();

        $response["can_change_position"] = DB::table('it_parameters as p')
            ->join('it_business as c', 'c.id', '=', 'p.it_business_id')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $MyUser["it_branches_id"])
            ->where('p.name', '=', 'change_positions')
            ->select('p.value as value')
            ->get();
        $response["can_change_position"] = $response["can_change_position"][0]->value;
        $response["expires_in"] = auth('api')->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;


        $plat = $request->header('X-Platform');

        if (!isset($plat) || empty($plat) || $plat != 'panel') {
            // $response["card_url"] = $this->make_bitly_url("http://card.getdc.us/?user=".$MyUser["id"]);
            $response["card_url"] = "http://card.getdc.us/?user=" . $MyUser["id"];
        }


        return response()->json($response);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $Auth_user = auth('api')->user();
        if (isset($Auth_user->id) && !empty($Auth_user->id)) {
            $token = $request->bearerToken();
            $MyUser = IT_User::find($Auth_user->id)->toArray();
            $response = array();
            $response['user'] = $MyUser;
            $response['token'] = $token;

            //$this->locale(Session::get('locale'), $request);

            $response = $this->clearResponse($MyUser);

            $response["company"] = DB::table('it_business as c')
                ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
                ->where('b.id', '=', $MyUser["it_branches_id"])
                ->select('c.id as id', 'c.alias as alias', 'c.logo as logo', 'c.name as name',  'c.backgroundcolor as backgroundcolor', 'c.textcolor as textcolor')
                ->get();

            $response["can_change_position"] = DB::table('it_parameters as p')
                ->join('it_business as c', 'c.id', '=', 'p.it_business_id')
                ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
                ->where('b.id', '=', $MyUser["it_branches_id"])
                ->where('p.name', '=', 'change_positions')
                ->select('p.value as value')
                ->get();
            $response["can_change_position"] = $response["can_change_position"][0]->value;

            $response["position"] = DB::table('it_positions')
                ->where('id', '=', $MyUser["it_positions_id"])
                ->select('id', 'name')
                ->get();


            $response["profile"] = DB::table('it_profiles')
                ->where('id', '=', $MyUser["it_profile_id"])
                ->select('id', 'level', 'name', 'it_business_id', 'status')
                ->get();


            $response["country"] = DB::table('it_countries')
                ->where('id', '=', $MyUser["it_countries_id"])
                ->select('id', 'iso')
                ->get();


            $plat = $request->header('X-Platform');

            if (!isset($plat) || empty($plat) || $plat != 'panel') {
                // $response["card_url"] = $this->make_bitly_url("http://card.getdc.us/?user=".$MyUser["id"]);
                $response["card_url"] = "http://card.getdc.us/?user=" . $MyUser["id"];
            }



            $id = DB::table('pruebasCarlosBeltan')->select('account_status')->where("id", "=", 1)->get();
            $response["account_status"] = $id[0]->account_status; //PaymentBlock, PaymentAlert, DemoAlert

            return response()->json($response);
        } else {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.token_invalid')]
                    ]
                ],
                401
            );
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->bearerToken();
        auth('api')->logout();

        return response()->json(['message' => trans('auth.logout')]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout_app(Request $request)
    {
        $token = $request->bearerToken();
        auth('api')->logout();

        return response()->json(['message' => trans('auth.logout')]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $old_token = $request->bearerToken();
        $new_token = auth('api')->refresh();
        $response = $this->respondWithToken($new_token);
        return $response;
    }

    public function invalidate()
    {
        #auth('api')->invalidate();
        // Pass true as the first param to force the token to be blacklisted "forever".
        auth('api')->invalidate(true);

        return response()->json(['messages' => [trans('auth.logout')]]);
    }


    private function check_active($login)
    {
        $count = IT_User::where('login', '=', $login)->select("status")->count();
        $item = IT_User::where('login', '=', $login)->select("status")->get();
        if ($count < 1) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.failed')]
                    ]
                ],
                401
            );
        } else if (!empty($item) && $item[0]->status == 'A') {
            return true;
        } else {
            return false;
        }
    }

    private function is_social($login, $request)
    {
        $count = IT_User::where('login', '=', $login)->select("status")->count();
        $item = IT_User::where('login', '=', $login)->select("session_type", "it_profile_id")->get();

        if ($count > 0) {
            $profile = Profile::where("id", $item[0]->it_profile_id)->select("level")->get();
            $profile = $profile[0];
        }


        $plat = $request->header('X-Platform');
        if ($count == 0) {

            return (response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.failed')]
                    ]
                ],
                401
            ));
        } else if ($plat == "panel" && $profile->level != '1') {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('auth.profileblocked')]
                    ]
                ],
                401
            );
        } else if (!empty($item) && $item[0]->session_type == 'S') {

            return "true";
        } else {
            return "false";
        }
    }



    public function make_bitly_url($url)
    {

        $bitly = 'https://api.bit.ly/shorten?version=2.0.1&amp;longUrl=' . urlencode($url) . '&amp;login=inmovsas&amp;apiKey=R_8eb5cc2d6d174a41bcb45eef81a3ab4a&amp;format=json';
        $response = file_get_contents($bitly);
        $json = json_decode($response, true);
        return $json['results'][$url]['shortUrl'];
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 20160
        ]);
    }

    protected function clearResponse($data)
    {
        unset($data["login_alt"], $data["password"], $data["platform"], $data["version"], $data["model"], $data["uuid"], $data["token"], $data["date_password_change"], $data["recovery_code"]);
        return $data;
    }



    protected function pushSms($message,  $numbers)
    {
        return   $this->_notifyThroughSms($message, $numbers);
    }

    private function _notifyThroughSms($e, $numbers)
    {
        // $items = $this->_notificationRecipients();


        foreach ($numbers  as $recipient) {
            return $this->_sendSms(
                $recipient["name"],
                $recipient["phone_number"],
                $e
            );
        }
    }

    private function _notificationRecipients()
    {
        $adminsFile = base_path() .
            DIRECTORY_SEPARATOR .
            'config' . DIRECTORY_SEPARATOR .
            'administrators.json';
        try {
            $adminsFileContents = \File::get($adminsFile);

            return json_decode($adminsFileContents);
        } catch (FileNotFoundException $e) {
            Log::error(
                'Could not find ' .
                    $adminsFile .
                    ' to notify admins through SMS'
            );
            return [];
        }
    }

    private function _sendSms($name, $to, $message)
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioNumber = env('TWILIO_NUMBER');

        $client = new Client($accountSid, $authToken);

        try {

            $message = $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

            sleep(4);
            // echo "----------------------------------------\n";
            $messageInfo = $client->messages($message->sid)
                ->fetch();
            $statusTW = $messageInfo->status;

            $twilioResponse = $statusTW;

            if (($twilioResponse != "undelivered") || ($twilioResponse != "failed")) {


                return response()->json(
                    [
                        'errors' => [
                            'status' => 200,
                            'messages' => [trans('auth.smssend', ['cel' => $this->phone_number])]
                        ]
                    ],
                    200
                );
            } else {


                switch ($twilioResponse) {
                    case "undelivered":
                        return response()->json(
                            [
                                'errors' => [
                                    'status' => 500,
                                    'messages' => [trans('auth.smsphoneoff')]
                                ]
                            ],
                            500
                        );
                        break;
                    case "unknown":
                    case "failed":
                        return response()->json(
                            [
                                'errors' => [
                                    'status' => 500,
                                    'messages' => [trans('auth.smsphonenoexist')]
                                ]
                            ],
                            500
                        );
                        break;
                }
                //sho wer
            }
            //   Log::info('Mensaje enviado a ' . $twilioNumber);
        } catch (TwilioException $e) {
            /* Log::error(
                'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e
            );*/

            return response()->json(
                [
                    'errors' => [
                        'status' => 500,
                        'messages' => [trans('auth.smsphonenoexist')]
                    ]
                ],
                500
            );
        }
    }
}
